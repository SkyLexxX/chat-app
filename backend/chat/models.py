from enum import Enum

from django.contrib.auth import get_user_model
from django.db import models

from backend.core.models import BaseModel
from backend.utils.hasher import generate_hash

User = get_user_model()


class Room(BaseModel):
    class RoomType(Enum):
        private_chat = "private"
        group_chat = "group"

    owner = models.ForeignKey(
        to=User,
        related_name="room_owner",
        related_query_name="room_owner",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    name = models.CharField(null=False, blank=False, max_length=60)
    type = models.CharField(
        max_length=22,
        choices=[(choice.name, choice.value) for choice in RoomType],
        default=RoomType.private_chat.value,
    )
    hash = models.CharField(
        null=False,
        blank=False,
        max_length=30,
        default=generate_hash,
    )

    def __str__(self):
        return f"{self.name} {self.type}"


class RoomMember(BaseModel):
    user = models.ForeignKey(
        to=User,
        related_name="room_member",
        related_query_name="room_member",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
    )
    room = models.ForeignKey(
        to=Room,
        related_name="room",
        related_query_name="room",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["user", "room"], name="unique user in a room"
            )
        ]

    def __str__(self):
        return f"{self.user} {self.room}"


class Message(BaseModel):
    sender = models.ForeignKey(
        to=User,
        related_name="message_sender",
        related_query_name="message_sender",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
    )
    room = models.ForeignKey(
        to=Room,
        related_name="message_room",
        related_query_name="message_room",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
    )
    content = models.TextField(blank=False, null=False)

    def __str__(self):
        return f"{self.sender} {self.room}"
