from asgiref.sync import async_to_sync
from channels.generic.websocket import JsonWebsocketConsumer
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.core.exceptions import ObjectDoesNotExist

from backend.chat.api.serializers import ChatMessageSerializer
from backend.chat.selectors import get_room_messages
from backend.chat.services import create_chat_message
from backend.users.selectors import get_user_by_id

User = get_user_model()


class ChatConsumer(JsonWebsocketConsumer):
    def load_messages(self, data):
        room_id = data.get("room_id")
        messages = get_room_messages(room_id=room_id)

        serializer_data = ChatMessageSerializer(messages, many=True).data

        content = {
            "command": "messages",
            "messages": serializer_data,
        }
        self.send_message(content)

    def new_message(self, data):
        room_id = data.get("room_id")
        sender_id = data.get("sender_id")
        sender = get_user_by_id(sender_id)

        message = create_chat_message(
            user=sender,
            id=room_id,
            message=data.get("message"),
        )

        serializer_data = ChatMessageSerializer(message, many=False).data

        content = {
            "command": "new_message",
            "message": serializer_data,
        }
        return self.send_chat_message(content)

    def echo(self, data):
        self.send_json(
            {
                "type": data.get("type"),
                "data": data.get("data"),
            }
        )

    commands = {
        "load_messages": load_messages,
        "new_message": new_message,
        "echo": echo,
    }

    def connect(self):
        user = self.scope.get("user", AnonymousUser())
        if user.is_anonymous:
            self.close()
        else:
            self.room_id = str(
                self.scope.get("url_route", {}).get("kwargs", {}).get("room_id")
            )

            async_to_sync(self.channel_layer.group_add)(self.room_id, self.channel_name)

            self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(self.room_id, self.channel_name)

    def receive_json(self, content, **kwargs):
        command = self.commands.get(content.get("command"))
        if command:
            command(self, content)
        else:
            raise ObjectDoesNotExist("Command does not exist")

    def send_chat_message(self, message):
        async_to_sync(self.channel_layer.group_send)(
            self.room_id,
            {"type": "chat_message", "message": message},
        )

    def send_message(self, message):
        self.send_json(content=message)

    def chat_message(self, event):
        message = event.get("message")
        self.send_message(message=message)

    def echo_message(self, message):
        self.send_json(
            {
                "type": message.get("type"),
                "data": message.get("data"),
            }
        )
