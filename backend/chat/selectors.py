import uuid
from typing import List, Optional

from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist

from backend.chat.models import Message, Room, RoomMember
from backend.utils.common import get_object_or_none

User = get_user_model()


def get_room_by_id(room_id: uuid.UUID) -> Optional[Room]:
    return get_object_or_none(Room, id=room_id)


def get_room_by_hash(room_hash: str) -> Optional[Room]:
    return get_object_or_none(Room, hash=room_hash)


def get_user_chats(user: User) -> List[Room]:
    rooms_id = [
        room_member.room.id
        for room_member in RoomMember.objects.filter(user=user).iterator()
    ]
    return Room.objects.filter(id__in=rooms_id)


def get_room_messages(room_id: uuid.UUID) -> Optional[List[Message]]:
    room = get_object_or_none(Room, id=room_id)
    if not room:
        raise ObjectDoesNotExist("Room does not exist")

    return Message.objects.filter(room=room)


def is_user_in_room(user: User, room: Room) -> bool:
    return RoomMember.objects.filter(user=user, room=room).exists()


def get_private_chat_with_recipient(user: User, recipient: User) -> Room:
    return (
        Room.objects.filter(
            room__user__in=[user, recipient], type=Room.RoomType.private_chat.name
        )
        .distinct()
        .first()
    )
