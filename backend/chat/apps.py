from django.apps import AppConfig


class ChatConfig(AppConfig):
    name = "backend.chat"
