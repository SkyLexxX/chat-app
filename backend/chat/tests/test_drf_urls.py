import json

import pytest
from django.urls import reverse

from backend.chat.models import Message, Room, RoomMember
from backend.utils.factories import (
    MessageFactory,
    RoomFactory,
    RoomMemberFactory,
    UserFactory,
)

pytestmark = pytest.mark.django_db

group_chat_room_url = "api:chat:group_chat_room"
invite_users_url = "api:chat:invite_users"
join_chat_url = "api:chat:join_chat"
user_chats_url = "api:chat:user_chats"
chat_messages_list_url = "api:chat:chat_messages_list"
send_chat_message_url = "api:chat:chat_message"


class TestGroupChatRoomApi:
    def test_create_group_chat(self, api_client_with_auth_header):
        client, user = api_client_with_auth_header
        test_users = UserFactory.create_batch(3)
        post_data = {
            "name": "test chat",
            "users": [str(user.id) for user in test_users],
        }

        resp = client.post(
            reverse(group_chat_room_url),
            json.dumps(post_data),
            content_type="application/json",
        )

        created_room = Room.objects.get(owner=user, name=post_data["name"])
        room_members_count = RoomMember.objects.filter(room=created_room).count()

        assert resp.status_code == 201
        assert resp.data.get("id") == str(created_room.id)
        assert resp.data.get("name") == created_room.name == post_data["name"]
        assert len(test_users) + 1 == room_members_count

    def test_create_group_chat_without_users(self, api_client_with_auth_header):
        client, user = api_client_with_auth_header
        post_data = {"name": "test chat"}

        resp = client.post(
            reverse(group_chat_room_url),
            json.dumps(post_data),
            content_type="application/json",
        )

        created_room = Room.objects.get(owner=user, name=post_data["name"])
        room_members_count = RoomMember.objects.filter(room=created_room).count()

        assert resp.status_code == 201
        assert resp.data.get("id") == str(created_room.id)
        assert resp.data.get("name") == created_room.name == post_data["name"]
        assert 1 == room_members_count

    def test_create_group_chat_with_unauthorized_user(self, api_client_no_auth):
        post_data = {"name": "test chat"}

        resp = api_client_no_auth.post(
            reverse(group_chat_room_url),
            json.dumps(post_data),
            content_type="application/json",
        )

        assert resp.status_code == 403


class TestInviteUsersApi:
    def test_invite_users(self, api_client_with_auth_header):
        client, user = api_client_with_auth_header
        test_room = RoomFactory(owner=user, name="test chat")
        test_users = UserFactory.create_batch(3)
        post_data = {
            "invite": test_room.hash,
            "users": [str(user.id) for user in test_users],
        }

        resp = client.post(
            reverse(invite_users_url),
            json.dumps(post_data),
            content_type="application/json",
        )

        room_members_count = RoomMember.objects.filter(room=test_room).count()

        assert resp.status_code == 200
        assert len(test_users) == room_members_count

    def test_invite_users_with_bad_invite(self, api_client_with_auth_header):
        client, user = api_client_with_auth_header
        test_room = RoomFactory(owner=user, name="test chat")
        test_users = UserFactory.create_batch(3)
        post_data = {
            "invite": test_room.hash[:-1],
            "users": [str(user.id) for user in test_users],
        }

        resp = client.post(
            reverse(invite_users_url),
            json.dumps(post_data),
            content_type="application/json",
        )

        room_members_count = RoomMember.objects.filter(room=test_room).count()

        assert resp.status_code == 404
        assert len(test_users) != room_members_count

    def test_invite_users_without_users(self, api_client_with_auth_header):
        client, user = api_client_with_auth_header
        test_room = RoomFactory(owner=user, name="test chat")
        post_data = {"invite": test_room.hash, "users": []}

        resp = client.post(
            reverse(invite_users_url),
            json.dumps(post_data),
            content_type="application/json",
        )

        assert resp.status_code == 404

    def test_invite_users_that_already_in_room(self, api_client_with_auth_header):
        client, user = api_client_with_auth_header
        test_room = RoomFactory(owner=user, name="test chat")
        test_users = UserFactory.create_batch(3)
        [RoomMemberFactory(user=user, room=test_room) for user in test_users]

        room_members_count = RoomMember.objects.filter(room=test_room).count()

        assert len(test_users) == room_members_count

        post_data = {
            "invite": test_room.hash,
            "users": [str(user.id) for user in test_users],
        }

        resp = client.post(
            reverse(invite_users_url),
            json.dumps(post_data),
            content_type="application/json",
        )

        room_members_count = RoomMember.objects.filter(room=test_room).count()

        assert resp.status_code == 200
        assert len(test_users) == room_members_count

    def test_invite_users_with_unauthorized_user(self, api_client_no_auth):
        test_room = RoomFactory(name="test chat")
        test_users = UserFactory.create_batch(3)

        post_data = {
            "invite": test_room.hash,
            "users": [str(user.id) for user in test_users],
        }

        resp = api_client_no_auth.post(
            reverse(invite_users_url),
            json.dumps(post_data),
            content_type="application/json",
        )

        assert resp.status_code == 403


class TestJoinChatApi:
    def test_join_chat(self, api_client_with_auth_header):
        client, user = api_client_with_auth_header
        test_room = RoomFactory(owner=user, name="test chat")
        post_data = {"invite": test_room.hash}

        resp = client.post(
            reverse(join_chat_url),
            json.dumps(post_data),
            content_type="application/json",
        )

        room_members_count = RoomMember.objects.filter(room=test_room).count()

        assert resp.status_code == 200
        assert 1 == room_members_count

    def test_join_chat_with_bad_invite(self, api_client_with_auth_header):
        client, user = api_client_with_auth_header
        test_room = RoomFactory(owner=user, name="test chat")
        post_data = {"invite": test_room.hash[:-1]}

        resp = client.post(
            reverse(join_chat_url),
            json.dumps(post_data),
            content_type="application/json",
        )

        room_members_count = RoomMember.objects.filter(room=test_room).count()

        assert resp.status_code == 404
        assert 0 == room_members_count

    def test_join_chat_if_already_in_it(self, api_client_with_auth_header):
        client, user = api_client_with_auth_header
        test_room = RoomFactory(owner=user, name="test chat")
        RoomMemberFactory(user=user, room=test_room)
        post_data = {"invite": test_room.hash}

        room_members_count = RoomMember.objects.filter(room=test_room).count()

        assert 1 == room_members_count

        resp = client.post(
            reverse(join_chat_url),
            json.dumps(post_data),
            content_type="application/json",
        )

        room_members_count = RoomMember.objects.filter(room=test_room).count()

        assert resp.status_code == 200
        assert 1 == room_members_count

    def test_join_chat_with_unauthorized_user(self, api_client_no_auth):
        test_room = RoomFactory(name="test chat")
        post_data = {"invite": test_room.hash}

        resp = api_client_no_auth.post(
            reverse(join_chat_url),
            json.dumps(post_data),
            content_type="application/json",
        )

        assert resp.status_code == 403


class TestUserChatsApi:
    def test_user_chat_list(self, api_client_with_auth_header):
        client, user = api_client_with_auth_header
        test_rooms = RoomFactory.create_batch(3)
        [RoomMemberFactory(user=user, room=room) for room in test_rooms]

        resp = client.get(
            reverse(user_chats_url),
            content_type="application/json",
        )

        assert resp.status_code == 200
        assert len(resp.data) == len(test_rooms)

    def test_user_chat_list_with_no_chats(self, api_client_with_auth_header):
        client, user = api_client_with_auth_header
        RoomFactory.create_batch(3)

        resp = client.get(
            reverse(user_chats_url),
            content_type="application/json",
        )

        assert resp.status_code == 200
        assert len(resp.data) == 0

    def test_chat_list_with_unauthorized_user(self, api_client_no_auth):
        RoomFactory.create_batch(3)

        resp = api_client_no_auth.get(
            reverse(user_chats_url),
            content_type="application/json",
        )

        assert resp.status_code == 403


class TestChatsMessagesListApi:
    def test_chat_messages(self, api_client_with_auth_header):
        client, user = api_client_with_auth_header
        test_room = RoomFactory()
        messages = MessageFactory.create_batch(5, room=test_room)

        resp = client.get(
            reverse(chat_messages_list_url, kwargs={"id": test_room.id}),
            content_type="application/json",
        )

        assert resp.status_code == 200
        assert len(resp.data) == len(messages)

    def test_chat_messages_with_bad_chat_id(self, api_client_with_auth_header):
        client, user = api_client_with_auth_header
        test_room = RoomFactory()
        MessageFactory.create_batch(5, room=test_room)
        bad_room_id = str(test_room.id)[:-3] + "098"

        resp = client.get(
            reverse(chat_messages_list_url, kwargs={"id": bad_room_id}),
            content_type="application/json",
        )

        assert resp.status_code == 404

    def test_chat_messages_with_no_messages(self, api_client_with_auth_header):
        client, user = api_client_with_auth_header
        test_room = RoomFactory()

        resp = client.get(
            reverse(chat_messages_list_url, kwargs={"id": test_room.id}),
            content_type="application/json",
        )

        assert resp.status_code == 200
        assert resp.data == []

    def test_chat_messages_with_unauthorized_user(self, api_client_no_auth):
        test_room = RoomFactory()

        resp = api_client_no_auth.get(
            reverse(chat_messages_list_url, kwargs={"id": test_room.id}),
            content_type="application/json",
        )

        assert resp.status_code == 403


class TestChatMessageApi:
    def test_send_private_chat_message(self, api_client_with_auth_header):
        client, user = api_client_with_auth_header
        test_user = UserFactory()
        post_data = {"content": "Message from 2021.07.27 00:16"}

        resp = client.post(
            reverse(send_chat_message_url, kwargs={"id": test_user.id}),
            json.dumps(post_data),
            content_type="application/json",
        )

        created_room = Room.objects.filter(type=Room.RoomType.private_chat.name).first()
        created_room_members = RoomMember.objects.filter(user__in=[user, test_user])
        created_message = Message.objects.filter(
            sender=user, room=created_room, content=post_data.get("content")
        ).exists()

        assert resp.status_code == 201
        assert created_room is not None
        assert created_room_members.count() == 2
        assert created_room_members[0].room == created_room
        assert created_room_members[1].room == created_room
        assert created_message is True

    def test_send_private_chat_message_to_existing_room(
        self, api_client_with_auth_header
    ):
        client, user = api_client_with_auth_header
        test_user = UserFactory()
        test_room = RoomFactory(type=Room.RoomType.private_chat.name)
        [RoomMemberFactory(user=user, room=test_room) for user in [user, test_user]]
        post_data = {"content": "Message from 2021.07.27 00:16"}

        resp = client.post(
            reverse(send_chat_message_url, kwargs={"id": test_user.id}),
            json.dumps(post_data),
            content_type="application/json",
        )

        created_room = Room.objects.filter(type=Room.RoomType.private_chat.name).first()
        created_room_members = RoomMember.objects.filter(user__in=[user, test_user])
        created_message = Message.objects.filter(
            sender=user, room=created_room, content=post_data.get("content")
        ).exists()

        assert resp.status_code == 201
        assert created_room is not None
        assert created_room_members.count() == 2
        assert created_room_members[0].room == created_room
        assert created_room_members[1].room == created_room
        assert created_message is True

    def test_send_group_chat_message(self, api_client_with_auth_header):
        client, user = api_client_with_auth_header
        test_users = UserFactory.create_batch(3)
        test_room = RoomFactory(name="test group", type=Room.RoomType.group_chat.name)
        [RoomMemberFactory(user=user, room=test_room) for user in test_users + [user]]
        post_data = {"content": "Message from 2021.07.27 00:37"}

        resp = client.post(
            reverse(send_chat_message_url, kwargs={"id": test_room.id}),
            json.dumps(post_data),
            content_type="application/json",
        )

        created_message = Message.objects.filter(
            sender=user, room=test_room, content=post_data.get("content")
        ).exists()

        assert resp.status_code == 201
        assert created_message is True

    def test_send_private_chat_message_with_unauthorized_user(self, api_client_no_auth):
        test_user = UserFactory()
        post_data = {"content": "Message from 2021.07.27 00:45"}

        resp = api_client_no_auth.post(
            reverse(send_chat_message_url, kwargs={"id": test_user.id}),
            json.dumps(post_data),
            content_type="application/json",
        )

        assert resp.status_code == 403

    def test_send_group_chat_message_with_unauthorized_user(self, api_client_no_auth):
        test_room = RoomFactory(name="test group", type=Room.RoomType.group_chat.name)
        post_data = {"content": "Message from 2021.07.27 00:45"}

        resp = api_client_no_auth.post(
            reverse(send_chat_message_url, kwargs={"id": test_room.id}),
            json.dumps(post_data),
            content_type="application/json",
        )

        assert resp.status_code == 403
