import uuid

import pytest
from django.core.exceptions import ObjectDoesNotExist

from backend.chat.models import Room
from backend.chat.selectors import (
    get_private_chat_with_recipient,
    get_room_by_hash,
    get_room_by_id,
    get_room_messages,
    get_user_chats,
    is_user_in_room,
)
from backend.utils.factories import (
    MessageFactory,
    RoomFactory,
    RoomMemberFactory,
    UserFactory,
)

pytestmark = pytest.mark.django_db


class TestGetRoomByIdSelector:
    def test_get_room_by_id(self):
        test_room = RoomFactory(name="test chat")

        expected = test_room
        result = get_room_by_id(test_room.id)

        assert expected == result

    def test_get_room_by_id_with_bad_id(self):
        RoomFactory(name="test chat")

        expected = None
        result = get_room_by_id(uuid.uuid4())

        assert expected == result


class TestGetRoomByHashSelector:
    def test_get_room_by_hash(self):
        test_room = RoomFactory(name="test chat")

        expected = test_room
        result = get_room_by_hash(test_room.hash)

        assert expected == result

    def test_get_room_by_hash_with_bad_hash(self):
        test_room = RoomFactory(name="test chat")
        bad_room_hash = test_room.hash[-3] + "123"

        expected = None
        result = get_room_by_hash(bad_room_hash)

        assert expected == result


class TestGetUserChatsSelector:
    def test_get_user_chats(self):
        user = UserFactory()
        test_rooms = RoomFactory.create_batch(3)
        [RoomMemberFactory(user=user, room=room) for room in test_rooms]

        expected = test_rooms
        result = list(get_user_chats(user))

        assert expected == result

    def test_get_user_chats_without_chats(self):
        user = UserFactory()
        RoomFactory.create_batch(3)

        expected = []
        result = list(get_user_chats(user))

        assert expected == result


class TestGetRoomMessagesSelector:
    def test_get_room_messages(self):
        test_room = RoomFactory()
        messages = MessageFactory.create_batch(5, room=test_room)

        expected = messages
        result = list(get_room_messages(test_room.id))

        assert expected == result

    def test_get_room_messages_with_bad_room_id(self):
        test_room = RoomFactory()
        MessageFactory.create_batch(5, room=test_room)

        with pytest.raises(ObjectDoesNotExist):
            get_room_messages(uuid.uuid4())

    def test_get_room_messages_without_messages(self):
        test_room = RoomFactory()

        expected = []
        result = list(get_room_messages(test_room.id))

        assert expected == result


class TestIsUserInRoomSelector:
    def test_is_user_in_room(self):
        test_user = UserFactory()
        test_room = RoomFactory()
        RoomMemberFactory(user=test_user, room=test_room)

        expected = True
        result = is_user_in_room(user=test_user, room=test_room)

        assert expected == result

    def test_is_user_in_room_fails(self):
        test_user = UserFactory()
        test_room = RoomFactory()

        expected = False
        result = is_user_in_room(user=test_user, room=test_room)

        assert expected == result


class TestGetPrivateChatWithRecipientSelector:
    def test_get_private_chat_with_recipient(self):
        test_private_room = RoomFactory(type=Room.RoomType.private_chat.name)
        test_group_room = RoomFactory(type=Room.RoomType.group_chat.name)
        test_user, test_recipient = UserFactory(), UserFactory()
        test_users = UserFactory.create_batch(3)
        [
            RoomMemberFactory(user=user, room=test_private_room)
            for user in [test_user, test_recipient]
        ]
        [RoomMemberFactory(user=user, room=test_group_room) for user in test_users]

        expected = test_private_room
        result = get_private_chat_with_recipient(
            user=test_user, recipient=test_recipient
        )

        assert result == expected

    def test_get_private_chat_with_recipient_without_room(self):
        test_group_room = RoomFactory(type=Room.RoomType.group_chat.name)
        test_user, test_recipient = UserFactory(), UserFactory()
        test_users = UserFactory.create_batch(3)
        [RoomMemberFactory(user=user, room=test_group_room) for user in test_users]

        expected = None
        result = get_private_chat_with_recipient(
            user=test_user, recipient=test_recipient
        )

        assert result == expected
