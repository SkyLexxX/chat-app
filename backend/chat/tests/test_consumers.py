import functools
import uuid

import pytest
from channels.db import database_sync_to_async
from channels.routing import URLRouter
from channels.testing import WebsocketCommunicator
from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.urls import path
from rest_framework_simplejwt.tokens import AccessToken

from backend.chat.api.serializers import ChatMessageSerializer
from backend.chat.consumers import ChatConsumer
from backend.chat.middleware import JWTAuthMiddlewareStack
from backend.chat.models import Message, Room
from backend.utils.factories import MessageFactory, RoomFactory, UserFactory

User = get_user_model()


@pytest.fixture(scope="module")
def chat_communicator():
    def build_communicator(access_token=None, test_room_id=None):
        application = JWTAuthMiddlewareStack(
            URLRouter(
                [
                    path(
                        "ws/chat/<uuid:room_id>/messages/",
                        ChatConsumer.as_asgi(),
                    ),
                ]
            )
        )

        communicator = WebsocketCommunicator(
            application,
            f"ws/chat/{test_room_id}/messages/",
            headers=[
                (b"authorization", access_token.encode() if access_token else b"")
            ],
        )

        return communicator

    return build_communicator


def clean_up_database(func):
    """
    Manually clean up created resources of the decorated function
    """

    @functools.wraps(func)
    async def wrapper_clean_up_database(*args, **kwargs):
        value = func(*args, **kwargs)
        await args[0].cleaner()
        return value

    return wrapper_clean_up_database


@pytest.mark.django_db
@pytest.mark.asyncio
class TestChatConsumer:
    @database_sync_to_async
    def cleaner(self):
        Room.objects.all().delete()
        User.objects.all().delete()
        Message.objects.all().delete()

    @database_sync_to_async
    def create_user_and_token(self):
        user = UserFactory()
        access_token = AccessToken.for_user(user)
        return user, str(access_token)

    @database_sync_to_async
    def create_messages_in_room(self, room):
        created_messages = []
        for user in UserFactory.create_batch(2):
            created_messages.extend(
                MessageFactory.create_batch(size=2, room=room, sender=user)
            )
        return created_messages

    @clean_up_database
    async def test_user_can_connect(self, chat_communicator):
        user, access_token = await self.create_user_and_token()
        test_room = await database_sync_to_async(RoomFactory)()
        communicator = chat_communicator(access_token, test_room.id)

        connected, subprotocol = await communicator.connect()
        assert connected

        await communicator.disconnect()

    @clean_up_database
    async def test_user_can_not_connect(self, chat_communicator):
        test_room = await database_sync_to_async(RoomFactory)()
        communicator = chat_communicator(test_room_id=test_room.id)

        connected, subprotocol = await communicator.connect()
        assert connected is False

    @clean_up_database
    async def test_user_can_send_and_receive_broadcast_messages(
        self, chat_communicator
    ):
        user, access_token = await self.create_user_and_token()
        test_room = await database_sync_to_async(RoomFactory)()
        communicator = chat_communicator(access_token, test_room.id)

        connected, subprotocol = await communicator.connect()
        assert connected

        from channels.layers import get_channel_layer

        channel_layer = get_channel_layer()

        message = {
            "type": "echo",
            "data": "This is a test message",
        }

        await channel_layer.group_send(str(test_room.id), message=message)
        response = await communicator.receive_json_from()
        assert response == message
        await communicator.disconnect()

    @clean_up_database
    async def test_load_messages(self, chat_communicator):
        user, access_token = await self.create_user_and_token()
        test_room = await database_sync_to_async(RoomFactory)()
        test_messages = await self.create_messages_in_room(room=test_room)
        communicator = chat_communicator(access_token, test_room.id)

        connected, subprotocol = await communicator.connect()
        assert connected

        await communicator.send_json_to(
            {
                "command": "load_messages",
                "room_id": str(test_room.id),
            }
        )

        expected = ChatMessageSerializer(test_messages, many=True).data
        response = await communicator.receive_json_from()
        assert response["messages"] == expected

        await communicator.disconnect()

    @clean_up_database
    async def test_load_messages_is_empty(self, chat_communicator):
        user, access_token = await self.create_user_and_token()
        test_room = await database_sync_to_async(RoomFactory)()
        communicator = chat_communicator(access_token, test_room.id)

        connected, subprotocol = await communicator.connect()
        assert connected

        await communicator.send_json_to(
            {
                "command": "load_messages",
                "room_id": str(test_room.id),
            }
        )

        response = await communicator.receive_json_from()
        assert response["messages"] == []

        await communicator.disconnect()

    @clean_up_database
    async def test_load_messages_with_bad_uuid(self, chat_communicator):
        user, access_token = await self.create_user_and_token()
        test_room = await database_sync_to_async(RoomFactory)()
        await self.create_messages_in_room(room=test_room)
        fake_uuid = uuid.uuid4()
        communicator = chat_communicator(access_token, test_room.id)

        connected, subprotocol = await communicator.connect()
        assert connected

        await communicator.send_json_to(
            {
                "command": "load_messages",
                "room_id": str(fake_uuid),
            }
        )

        with pytest.raises(ObjectDoesNotExist):
            await communicator.receive_json_from()

    @clean_up_database
    async def test_send_new_message(self, chat_communicator):
        user, access_token = await self.create_user_and_token()
        test_room = await database_sync_to_async(RoomFactory)()
        communicator = chat_communicator(access_token, test_room.id)

        connected, subprotocol = await communicator.connect()
        assert connected

        payload = {
            "room_id": str(test_room.id),
            "sender_id": str(user.id),
            "message": "test-message",
            "command": "new_message",
        }

        await communicator.send_json_to(payload)

        response = await communicator.receive_json_from()

        assert response["command"] == payload["command"]
        assert response["message"]["content"] == payload["message"]
        assert response["message"]["room"] == str(test_room)
        assert response["message"]["sender"] == str(user)
        assert response["message"]["id"] is not None

        await communicator.disconnect()

    @clean_up_database
    async def test_send_new_message_with_bad_room_id(self, chat_communicator):
        fake_uuid = uuid.uuid4()
        user, access_token = await self.create_user_and_token()
        communicator = chat_communicator(access_token, fake_uuid)

        connected, subprotocol = await communicator.connect()
        assert connected

        payload = {
            "room_id": str(fake_uuid),
            "sender_id": str(user.id),
            "message": "test-message",
            "command": "new_message",
        }

        await communicator.send_json_to(payload)

        with pytest.raises(ObjectDoesNotExist):
            await communicator.receive_json_from()

    @clean_up_database
    async def test_send_new_message_with_bad_user_id(self, chat_communicator):
        fake_uuid = uuid.uuid4()
        user, access_token = await self.create_user_and_token()
        test_room = await database_sync_to_async(RoomFactory)()
        communicator = chat_communicator(access_token, test_room.id)

        connected, subprotocol = await communicator.connect()
        assert connected

        payload = {
            "room_id": str(test_room.id),
            "sender_id": str(fake_uuid),
            "message": "test-message",
            "command": "new_message",
        }

        await communicator.send_json_to(payload)

        with pytest.raises(ObjectDoesNotExist):
            await communicator.receive_json_from()

    @clean_up_database
    async def test_send_bad_command(self, chat_communicator):
        fake_uuid = uuid.uuid4()
        user, access_token = await self.create_user_and_token()
        communicator = chat_communicator(access_token, fake_uuid)

        connected, subprotocol = await communicator.connect()
        assert connected

        payload = {"command": "random_command"}

        await communicator.send_json_to(payload)

        with pytest.raises(ObjectDoesNotExist):
            await communicator.receive_json_from()
