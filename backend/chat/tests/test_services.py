import pytest
from django.core.exceptions import ObjectDoesNotExist

from backend.chat.models import Message, Room, RoomMember
from backend.chat.services import (
    create_chat_message,
    create_chat_room,
    create_room_members,
)
from backend.utils.factories import RoomFactory, RoomMemberFactory, UserFactory

pytestmark = pytest.mark.django_db


class TestCreateChatRoomService:
    def test_create_chat_room_with_private_type(self):
        result = create_chat_room(room_type=Room.RoomType.private_chat.name)
        expected = Room.objects.filter(type=Room.RoomType.private_chat.name).first()

        assert result == expected

    def test_create_chat_room_with_group_type(self):
        test_user = UserFactory()
        result = create_chat_room(
            room_type=Room.RoomType.group_chat.name, owner=test_user, name="test name"
        )
        expected = Room.objects.filter(
            type=Room.RoomType.group_chat.name, owner=test_user, name="test name"
        ).first()

        assert result == expected

    def test_create_chat_room_with_bad_arguments(self):
        with pytest.raises(TypeError):
            create_chat_room(
                room_type=Room.RoomType.group_chat.name, bad_arg="argument"
            )


class TestCreateRoomMembersService:
    def test_create_room_members(self, mocker):
        test_users = UserFactory.create_batch(3)
        test_room = RoomFactory(name="test chat")

        mocker.patch("backend.chat.services.is_user_in_room", return_value=False)

        result = create_room_members(users=test_users, room=test_room)

        expected_room_members = RoomMember.objects.filter(
            user__in=test_users, room=test_room
        )

        assert set(result) == set(expected_room_members)
        assert len(result) == expected_room_members.count()

    def test_create_room_members_without_room(self):
        test_users = UserFactory.create_batch(3)
        test_room = None

        with pytest.raises(ObjectDoesNotExist):
            create_room_members(users=test_users, room=test_room)

    def test_create_room_members_without_users(self):
        test_users = []
        test_room = RoomFactory(name="test chat")

        with pytest.raises(ObjectDoesNotExist):
            create_room_members(users=test_users, room=test_room)

    def test_create_room_members_without_room_and_users(self):
        test_users = []
        test_room = None

        with pytest.raises(ObjectDoesNotExist):
            create_room_members(users=test_users, room=test_room)

    def test_create_room_members_with_user_in_room(self):
        test_users = UserFactory.create_batch(3)
        test_room = RoomFactory(name="test chat")
        RoomMemberFactory(user=test_users[0], room=test_room)

        result = create_room_members(users=test_users, room=test_room)

        expected_room_members = RoomMember.objects.filter(
            user__in=test_users, room=test_room
        ).exclude(user=test_users[0])

        assert set(result) == set(expected_room_members)
        assert len(result) == expected_room_members.count()


class TestSendChatMessageService:
    def test_private_send_chat_message(self, mocker):
        test_user = UserFactory()
        test_recipient = UserFactory()
        test_message = "Hello from 29.07.2021 22:09"

        mocker.patch("backend.chat.services.get_room_by_id", return_value=None)
        mocker.patch(
            "backend.chat.services.get_user_by_id", return_value=test_recipient
        )
        mocker.patch(
            "backend.chat.services.get_private_chat_with_recipient", return_value=None
        )

        result = create_chat_message(
            user=test_user, id=test_recipient.id, message=test_message
        )

        expected_room = Room.objects.filter(
            type=Room.RoomType.private_chat.name
        ).first()
        expected_room_members = RoomMember.objects.filter(
            user__in=[test_user, test_recipient], room=expected_room
        ).count()
        expected_message = Message.objects.filter(
            sender=test_user, room=expected_room, content=test_message
        ).first()

        assert 2 == expected_room_members
        assert result == expected_message

    def test_private_send_chat_message_with_existing_room(self, mocker):
        test_user = UserFactory()
        test_recipient = UserFactory()
        test_room = RoomFactory()
        [
            RoomMemberFactory(user=user, room=test_room)
            for user in [test_user, test_recipient]
        ]
        test_message = "Hello from 29.07.2021 22:09"

        mocker.patch("backend.chat.services.get_room_by_id", return_value=None)
        mocker.patch(
            "backend.chat.services.get_user_by_id", return_value=test_recipient
        )
        mocker.patch(
            "backend.chat.services.get_private_chat_with_recipient",
            return_value=test_room,
        )

        result = create_chat_message(
            user=test_user, id=test_recipient.id, message=test_message
        )

        expected_message = Message.objects.filter(
            sender=test_user, room=test_room, content=test_message
        ).first()

        assert result == expected_message

    def test_group_send_chat_message(self, mocker):
        test_user = UserFactory()
        test_room = RoomFactory(name="test chat")
        [
            RoomMemberFactory(user=user, room=test_room)
            for user in UserFactory.create_batch(2)
        ]
        test_message = "Hello from 29.07.2021 22:32"

        mocker.patch("backend.chat.services.get_room_by_id", return_value=test_room)

        result = create_chat_message(
            user=test_user, id=test_room.id, message=test_message
        )

        expected_message = Message.objects.filter(
            sender=test_user, room=test_room, content=test_message
        ).first()

        assert result == expected_message
