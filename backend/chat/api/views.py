from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import OpenApiParameter, extend_schema
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED
from rest_framework.views import APIView

from backend.chat.api.serializers import (
    ChatMessageInputSerializer,
    ChatMessageSerializer,
    ChatRoomSerializer,
    GroupChatRoomInputSerializer,
    InviteUserSerializer,
    JoinChatSerializer,
    UserChatsSerializer,
)
from backend.chat.models import Room
from backend.chat.selectors import get_room_by_hash, get_room_messages, get_user_chats
from backend.chat.services import (
    create_chat_message,
    create_chat_room,
    create_room_members,
)
from backend.utils.mixins import ApiAuthMixin, ApiErrorsMixin


class GroupChatRoomView(ApiAuthMixin, ApiErrorsMixin, APIView):
    @extend_schema(
        request=GroupChatRoomInputSerializer,
        responses={201: ChatRoomSerializer},
    )
    def post(self, request, *args, **kwargs):
        input_serializer = GroupChatRoomInputSerializer(data=request.data)
        input_serializer.is_valid(raise_exception=True)

        chat_room = create_chat_room(
            room_type=Room.RoomType.group_chat.name,
            owner=request.user,
            name=input_serializer.validated_data.get("name"),
        )

        users = [request.user]
        users.extend(input_serializer.validated_data.get("users", []))

        create_room_members(users=users, room=chat_room)

        chat_serializer = ChatRoomSerializer(chat_room, many=False).data
        return Response(chat_serializer, status=HTTP_201_CREATED)


class InviteUsersView(ApiAuthMixin, ApiErrorsMixin, APIView):
    serializer_class = InviteUserSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        create_room_members(
            users=serializer.validated_data.get("users"),
            room=get_room_by_hash(serializer.validated_data.get("invite")),
        )

        return Response({"status": "OK"}, status=HTTP_200_OK)


class JoinChatView(ApiAuthMixin, ApiErrorsMixin, APIView):
    serializer_class = JoinChatSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        create_room_members(
            users=[request.user],
            room=get_room_by_hash(serializer.validated_data.get("invite")),
        )

        return Response({"status": "OK"}, status=HTTP_200_OK)


class UserChatsView(ApiAuthMixin, ApiErrorsMixin, APIView):
    serializer_class = UserChatsSerializer

    def get(self, request, *args, **kwargs):
        chats = get_user_chats(request.user)
        serializer = self.serializer_class(chats, many=True)
        return Response(serializer.data)


class ChatMessagesView(ApiAuthMixin, ApiErrorsMixin, generics.ListAPIView):
    serializer_class = ChatMessageSerializer

    def get_queryset(self):
        return get_room_messages(room_id=self.kwargs.get("id"))


class ChatMessageView(ApiAuthMixin, ApiErrorsMixin, APIView):
    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="id",
                description="User or Room UUID",
                required=True,
                type=OpenApiTypes.UUID,
                location=OpenApiParameter.PATH,
            ),
        ],
        request=ChatMessageInputSerializer,
        responses={201: ChatMessageSerializer},
    )
    def post(self, request, *args, **kwargs):
        serializer = ChatMessageInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        message = create_chat_message(
            user=request.user,
            id=kwargs.get("id"),
            message=serializer.data.get("content"),
        )

        data = ChatMessageSerializer(message, many=False).data
        return Response(data, status=HTTP_201_CREATED)
