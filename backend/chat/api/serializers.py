from django.contrib.auth import get_user_model
from rest_framework import serializers

from backend.chat.models import Message, Room

User = get_user_model()


class GroupChatRoomInputSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=60, required=True)
    users = serializers.PrimaryKeyRelatedField(
        many=True, queryset=User.objects.all(), write_only=True, required=False
    )


class ChatRoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = ["id", "name"]


class InviteUserSerializer(serializers.Serializer):
    invite = serializers.CharField(write_only=True, required=True)
    users = serializers.PrimaryKeyRelatedField(
        many=True, queryset=User.objects.all(), write_only=True, required=True
    )


class JoinChatSerializer(serializers.Serializer):
    invite = serializers.CharField(write_only=True, required=True)


class UserChatsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = ["id", "name"]


class ChatMessageSerializer(serializers.ModelSerializer):
    sender = serializers.UUIDField()
    room = serializers.UUIDField()

    class Meta:
        model = Message
        fields = ["id", "sender", "room", "content"]


class ChatMessageInputSerializer(serializers.Serializer):
    content = serializers.CharField(required=True)
