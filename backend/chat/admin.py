from django.contrib import admin

from backend.chat.models import Message, Room, RoomMember

admin.site.register(Room)
admin.site.register(RoomMember)
admin.site.register(Message)
