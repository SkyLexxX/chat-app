from django.urls import path

from backend.chat.consumers import ChatConsumer

websocket_urlpatterns = [
    path(
        "ws/chat/<uuid:room_id>/messages/", ChatConsumer.as_asgi(), name="chat_consumer"
    ),
]
