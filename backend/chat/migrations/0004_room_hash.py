# Generated by Django 3.1.12 on 2021-07-21 18:48

import backend.utils.hasher
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0003_auto_20210721_1726'),
    ]

    operations = [
        migrations.AddField(
            model_name='room',
            name='hash',
            field=models.URLField(default=backend.utils.hasher.generate_hash),
        ),
    ]
