# Generated by Django 3.1.12 on 2021-07-21 17:26

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('chat', '0002_auto_20210719_1531'),
    ]

    operations = [
        migrations.AddField(
            model_name='room',
            name='owner',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='room_owner', related_query_name='room_owner', to='users.user'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='room',
            name='type',
            field=models.CharField(choices=[('private_chat', 'private'), ('group_chat', 'group')], default='private', max_length=22),
        ),
    ]
