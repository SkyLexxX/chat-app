import uuid
from typing import List, Optional

from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist

from backend.chat.models import Message, Room, RoomMember
from backend.chat.selectors import (
    get_private_chat_with_recipient,
    get_room_by_id,
    is_user_in_room,
)
from backend.users.selectors import get_user_by_id

User = get_user_model()


def create_chat_room(room_type: Room.RoomType, **kwargs) -> Room:
    return Room.objects.create(type=room_type, **kwargs)


def create_room_members(users: List, room: Room) -> Optional[List[RoomMember]]:
    if users and room:
        members = [
            RoomMember(user=user, room=room)
            for user in users
            if not is_user_in_room(user=user, room=room)
        ]
        return RoomMember.objects.bulk_create(members)
    elif not room:
        raise ObjectDoesNotExist("Room does not exist")
    else:
        raise ObjectDoesNotExist("Object does not exist")


def create_chat_message(user: User, id: uuid.UUID, message: str) -> Optional[Message]:
    if not user:
        raise ObjectDoesNotExist("User does not exist")
    room = get_room_by_id(room_id=id)
    if room:
        return Message.objects.create(sender=user, room=room, content=message)

    recipient = get_user_by_id(user_id=id)
    if recipient:
        room = get_private_chat_with_recipient(user, recipient)
        if not room:
            room = Room.objects.create(type=Room.RoomType.private_chat.name)
            create_room_members(users=[user, recipient], room=room)
        return Message.objects.create(sender=user, room=room, content=message)
    raise ObjectDoesNotExist("Provided uuid is invalid")
