from django.urls import path

from backend.chat.api.views import (
    ChatMessagesView,
    ChatMessageView,
    GroupChatRoomView,
    InviteUsersView,
    JoinChatView,
    UserChatsView,
)

app_name = "chat"
urlpatterns = [
    path("room/", GroupChatRoomView.as_view(), name="group_chat_room"),
    path("invite/users/", InviteUsersView.as_view(), name="invite_users"),
    path("join-chat/", JoinChatView.as_view(), name="join_chat"),
    path("chats/", UserChatsView.as_view(), name="user_chats"),
    path("message/<uuid:id>/", ChatMessageView.as_view(), name="chat_message"),
    path("<uuid:id>/messages/", ChatMessagesView.as_view(), name="chat_messages_list"),
]
