from django.apps import AppConfig


class HealthCheckConfig(AppConfig):
    name = 'backend.health_check'
