import datetime

from django.http import HttpResponse
from django.views.generic import View


class HealthCheckView(View):
    @staticmethod
    def get(request, *args, **kwargs):
        now = datetime.datetime.now()
        style = (
            "width: 100%;"
            "height: 100%;"
            "display: flex;"
            "align-items: center;"
            "justify-content: center;"
        )
        html = f"<html><body><h1 style='{style}'>It is now {now.strftime('%c')}.</h1></body></html>"
        return HttpResponse(html, status=200)
