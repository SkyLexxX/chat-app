import pytest
from celery.result import EagerResult

from backend.users.tasks import get_users_count
from backend.utils.factories import UserFactory

pytestmark = pytest.mark.django_db


class TestUserCountTask:
    def test_user_count(self, settings):
        """A basic test to execute the get_users_count Celery task."""
        UserFactory.create_batch(3)
        settings.CELERY_TASK_ALWAYS_EAGER = True
        task_result = get_users_count.delay()
        assert isinstance(task_result, EagerResult)
        # NOTE +1 user because of pytest.fixture create_user
        assert task_result.result == 4
