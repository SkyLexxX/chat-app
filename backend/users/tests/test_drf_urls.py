import json
import uuid

import pytest
from django.urls import reverse

from backend.users.models import Contact, User
from backend.utils.factories import ContactFactory, UserFactory

pytestmark = pytest.mark.django_db

register_url = "api:users:register"
login_url = "api:users:token_obtain_pair"
token_refresh_url = "api:users:token_refresh"
user_detail_url = "api:users:user_detail"
user_update_url = "api:users:user_update"
add_contact_url = "api:users:add_contact"
user_contacts_url = "api:users:user_contacts"


class TestRegisterApi:
    def test_user_registration(self, api_client_no_auth):
        post_data = {
            "email": "testuser3@gmail.com",
            "password": "testpassword",
        }

        resp = api_client_no_auth.post(
            reverse(register_url),
            json.dumps(post_data),
            content_type="application/json",
        )

        created_user = User.objects.get(email="testuser3@gmail.com")

        assert resp.status_code == 201
        assert created_user is not None

    def test_user_registration_with_user_details(self, api_client_no_auth):
        post_data = {
            "first_name": "test",
            "last_name": "user",
            "username": "randomu",
            "email": "testuser3@gmail.com",
            "password": "testpassword",
        }

        resp = api_client_no_auth.post(
            reverse(register_url),
            json.dumps(post_data),
            content_type="application/json",
        )

        created_user = User.objects.get(email="testuser3@gmail.com")

        assert resp.status_code == 201
        assert created_user is not None
        assert created_user.first_name == post_data.get("first_name")
        assert created_user.last_name == post_data.get("last_name")
        assert created_user.username == post_data.get("username")


class TestLoginApi:
    def test_user_login(self, api_client_no_auth):
        UserFactory(email="username@gmail.com", password="password")
        post_data = {"email": "username@gmail.com", "password": "password"}

        resp = api_client_no_auth.post(
            reverse(login_url), json.dumps(post_data), content_type="application/json"
        )

        assert resp.status_code == 200
        assert resp.data["access"], resp.data["refresh"] is not None


class TestTokenRefreshApi:
    def test_token_refresh(self, api_client_no_auth):
        UserFactory(email="username@gmail.com", password="password")
        post_data = {"email": "username@gmail.com", "password": "password"}

        resp = api_client_no_auth.post(
            reverse(login_url), json.dumps(post_data), content_type="application/json"
        )

        access, refresh = resp.data["access"], resp.data["refresh"]

        resp = api_client_no_auth.post(
            reverse(token_refresh_url),
            json.dumps({"refresh": refresh}),
            content_type="application/json",
        )

        assert resp.status_code == 200
        assert resp.data["access"] != access


class TestUserDetailApi:
    def test_user_detail(self, api_client_with_auth_header):
        client, user = api_client_with_auth_header

        resp = client.get(
            reverse(user_detail_url),
            content_type="application/json",
        )

        assert resp.status_code == 200
        assert resp.data is not None

    def test_user_detail_with_unauthorized_user(self, api_client_no_auth):
        resp = api_client_no_auth.get(
            reverse(user_detail_url),
            content_type="application/json",
        )

        assert resp.status_code == 403
        assert resp.data["detail"] is not None


class TestUserUpdateApi:
    def test_user_update(self, api_client_with_auth_header):
        client, user = api_client_with_auth_header
        patch_data = {"username": "SkyLexxX"}

        resp = client.patch(
            reverse(user_update_url),
            json.dumps(patch_data),
            content_type="application/json",
        )

        assert resp.status_code == 200
        assert resp.data["username"] == patch_data["username"]

    def test_user_update_with_unauthorized_user(self, api_client_no_auth):
        patch_data = {"username": "SkyLexxX"}

        resp = api_client_no_auth.patch(
            reverse(user_update_url),
            json.dumps(patch_data),
            content_type="application/json",
        )

        assert resp.status_code == 403
        assert resp.data["detail"] is not None


class TestAddContactApi:
    def test_add_contact(self, api_client_with_auth_header):
        client, user = api_client_with_auth_header
        test_friend = UserFactory()
        post_data = {"friend": str(test_friend.id)}

        resp = client.post(
            reverse(add_contact_url),
            json.dumps(post_data),
            content_type="application/json",
        )

        created_contact = Contact.objects.filter(user=user, friend=test_friend).exists()

        assert resp.status_code == 200
        assert created_contact is True

    def test_add_contact_with_existing_contact(self, api_client_with_auth_header):
        client, user = api_client_with_auth_header
        test_friend = UserFactory()
        ContactFactory(user=user, friend=test_friend)
        post_data = {"friend": str(test_friend.id)}

        resp = client.post(
            reverse(add_contact_url),
            json.dumps(post_data),
            content_type="application/json",
        )

        assert resp.status_code == 400

    def test_add_contact_with_non_existing_user(self, api_client_with_auth_header):
        client, user = api_client_with_auth_header
        post_data = {"friend": str(uuid.UUID)}

        resp = client.post(
            reverse(add_contact_url),
            json.dumps(post_data),
            content_type="application/json",
        )

        assert resp.status_code == 400

    def test_add_contact_with_unauthorized_user(self, api_client_no_auth):
        test_friend = UserFactory()
        post_data = {"friend": str(test_friend.id)}

        resp = api_client_no_auth.patch(
            reverse(add_contact_url),
            json.dumps(post_data),
            content_type="application/json",
        )

        assert resp.status_code == 403


class TestUserContactsApi:
    def test_user_contacts(self, api_client_with_auth_header):
        client, user = api_client_with_auth_header
        ContactFactory.create_batch(3, user=user)

        resp = client.get(
            reverse(user_contacts_url),
            content_type="application/json",
        )

        assert resp.status_code == 200
        assert len(resp.data) == 3

    def test_user_contacts_without_contacts(self, api_client_with_auth_header):
        client, user = api_client_with_auth_header

        resp = client.get(
            reverse(user_contacts_url),
            content_type="application/json",
        )

        assert resp.status_code == 200
        assert resp.data == []

    def test_user_contacts_with_unauthorized_user(self, api_client_no_auth):
        resp = api_client_no_auth.get(
            reverse(user_contacts_url),
            content_type="application/json",
        )

        assert resp.status_code == 403
