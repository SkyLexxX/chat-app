import pytest
from django.core.exceptions import ValidationError

from backend.users.models import Contact
from backend.users.services import user_add_contact, user_update, validate_user_contact
from backend.utils.factories import UserFactory

pytestmark = pytest.mark.django_db


class TestUserAddContactService:
    def test_user_add_contact(self, mocker):
        test_user = UserFactory()
        test_friend = UserFactory()

        mocker.patch("backend.users.services.validate_user_contact", return_value=False)

        result = user_add_contact(user=test_user, friend=test_friend)
        expected = Contact.objects.filter(user=test_user, friend=test_friend).first()

        assert result == expected

    def test_user_add_contact_with_existing_contact(self, mocker):
        test_user = UserFactory()
        test_friend = UserFactory()

        mocker.patch("backend.users.services.is_contact_exist", return_value=True)

        with pytest.raises(ValidationError):
            user_add_contact(user=test_user, friend=test_friend)


class TestValidateUserContactService:
    def test_validate_user_contact(self, mocker):
        test_user = UserFactory()
        test_friend = UserFactory()

        mocker.patch("backend.users.services.is_contact_exist", return_value=False)

        expected = False
        result = validate_user_contact(user=test_user, friend=test_friend)

        assert result == expected

    def test_validate_user_contact_with_existing_contact(self, mocker):
        test_user = UserFactory()
        test_friend = UserFactory()

        mocker.patch("backend.users.services.is_contact_exist", return_value=True)

        with pytest.raises(ValidationError):
            validate_user_contact(user=test_user, friend=test_friend)


class TestUserUpdateService:
    def test_user_update(self):
        test_user = UserFactory()
        payload = {
            "username": "SkyLexxX",
            "first_name": "new_name",
            "last_name": "new_name",
        }

        result = user_update(user=test_user, **payload)

        assert result.username == payload.get("username")
        assert result.first_name == payload.get("first_name")
        assert result.last_name == payload.get("last_name")

    def test_user_update_without_data(self):
        test_user = UserFactory()

        result = user_update(user=test_user)
        expected = test_user

        assert result == expected
