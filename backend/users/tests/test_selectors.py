import uuid

import pytest

from backend.users.selectors import get_user_by_id, is_contact_exist
from backend.utils.factories import ContactFactory, UserFactory

pytestmark = pytest.mark.django_db


class TestGetUserByIdSelector:
    def test_get_user_by_id(self):
        test_user = UserFactory()

        expected = test_user
        result = get_user_by_id(test_user.id)

        assert expected == result

    def test_get_room_by_id_with_bad_id(self):
        UserFactory()

        expected = None
        result = get_user_by_id(uuid.uuid4())

        assert expected == result


class TestIsContactExistSelector:
    def test_is_contact_exist(self):
        test_user = UserFactory()
        test_friend = UserFactory()
        ContactFactory(user=test_user, friend=test_friend)

        expected = True
        result = is_contact_exist(user=test_user, friend=test_friend)

        assert expected == result

    def test_is_contact_exist_fails(self):
        test_user = UserFactory()
        test_friend = UserFactory()

        expected = False
        result = is_contact_exist(user=test_user, friend=test_friend)

        assert expected == result
