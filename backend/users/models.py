from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models

from backend.core.models import BaseModel
from backend.users.managers import UserManager


class User(AbstractBaseUser, BaseModel, PermissionsMixin):
    first_name = models.CharField(blank=True, max_length=255)
    last_name = models.CharField(blank=True, max_length=255)
    username = models.CharField(blank=True, max_length=255)
    email = models.EmailField(verbose_name="email", max_length=60, unique=True)

    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    USERNAME_FIELD = "email"

    objects = UserManager()

    def __str__(self):
        return self.email


class Contact(BaseModel):
    user = models.ForeignKey(
        to=User,
        related_name="contact_user",
        related_query_name="contact_user",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
    )
    friend = models.ForeignKey(
        to=User,
        related_name="contact_friend",
        related_query_name="contact_friend",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=["user", "friend"], name="unique friend")
        ]

    def __str__(self):
        return f"{self.user} => {self.friend}"
