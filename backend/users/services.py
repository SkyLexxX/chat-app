from typing import Optional

from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError

from backend.users.models import Contact
from backend.users.selectors import is_contact_exist

User = get_user_model()


def user_add_contact(user: User, friend: User) -> Contact:
    validate_user_contact(user=user, friend=friend)
    return Contact.objects.create(user=user, friend=friend)


def user_update(user: User, **kwargs) -> User:
    for key, value in kwargs.items():
        setattr(user, key, value)
    return user


def validate_user_contact(user: User, friend: User) -> Optional[bool]:
    if is_contact_exist(user, friend):
        raise ValidationError("Contact already exist")
    return False
