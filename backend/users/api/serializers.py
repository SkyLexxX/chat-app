from django.contrib.auth import get_user_model
from rest_framework import serializers

from backend.users.models import Contact

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["first_name", "last_name", "username", "email"]


class UserRegisterSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=128, min_length=8, write_only=True)

    class Meta:
        model = User
        fields = ["first_name", "last_name", "username", "email", "password"]
        extra_kwargs = {
            "first_name": {"required": False},
            "last_name": {"required": False},
            "username": {"required": False},
        }

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)


class UserContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = ["user", "friend"]
        extra_kwargs = {"user": {"read_only": True}}
