from django.contrib.auth import get_user_model
from django.dispatch import receiver
from django_rest_passwordreset.signals import reset_password_token_created
from rest_framework import generics, permissions, response
from rest_framework.views import APIView

from backend.users.api.serializers import (
    UserContactSerializer,
    UserRegisterSerializer,
    UserSerializer,
)
from backend.users.models import Contact
from backend.users.services import user_add_contact, user_update
from backend.utils.mailer import EmailService
from backend.utils.mixins import ApiAuthMixin, ApiErrorsMixin

User = get_user_model()


class UserView(ApiAuthMixin, ApiErrorsMixin, APIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def get(self, request):
        serializer = self.serializer_class(request.user)
        return response.Response(serializer.data)


class UserPartialUpdateView(ApiAuthMixin, ApiErrorsMixin, APIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def patch(self, request, *args, **kwargs):
        input_serializer = self.serializer_class(data=request.data, partial=True)
        input_serializer.is_valid(raise_exception=True)

        updated_user = user_update(user=request.user, **input_serializer.validated_data)

        output_serializer = self.serializer_class(updated_user).data
        return response.Response(output_serializer)


class UserRegisterView(ApiErrorsMixin, generics.CreateAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = UserRegisterSerializer


@receiver(reset_password_token_created)
def password_reset_token_created(
    sender, instance, reset_password_token, *args, **kwargs
):
    EmailService.send_reset_password_email(
        [reset_password_token.user.email], reset_password_token.key
    )


class UserAddContactView(ApiAuthMixin, ApiErrorsMixin, APIView):
    serializer_class = UserContactSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        contact = user_add_contact(
            user=request.user, friend=serializer.validated_data.get("friend")
        )

        serializer = self.serializer_class(contact).data
        return response.Response(serializer)


class UserContactsView(ApiAuthMixin, ApiErrorsMixin, generics.ListAPIView):
    serializer_class = UserContactSerializer
    queryset = Contact.objects.all()
