from django.urls import include, path
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from backend.users.api.views import (
    UserAddContactView,
    UserContactsView,
    UserPartialUpdateView,
    UserRegisterView,
    UserView,
)

app_name = "users"
urlpatterns = [
    path("me/", UserView.as_view(), name="user_detail"),
    path("update/", UserPartialUpdateView.as_view(), name="user_update"),
    path("register/", UserRegisterView.as_view(), name="register"),
    path("login/", TokenObtainPairView.as_view(), name="token_obtain_pair"),
    path("token-refresh/", TokenRefreshView.as_view(), name="token_refresh"),
    path(
        "reset_password/",
        include("django_rest_passwordreset.urls", namespace="password_reset"),
    ),
    path("add-contact/", UserAddContactView.as_view(), name="add_contact"),
    path("contacts/", UserContactsView.as_view(), name="user_contacts"),
]
