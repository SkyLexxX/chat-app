import uuid
from typing import Optional

from django.contrib.auth import get_user_model

from backend.users.models import Contact
from backend.utils.common import get_object_or_none

User = get_user_model()


def get_user_by_id(user_id: uuid.UUID) -> Optional[User]:
    return get_object_or_none(User, id=user_id)


def is_contact_exist(user: User, friend: User) -> bool:
    return Contact.objects.filter(user=user, friend=friend).exists()
