from typing import Any, Sequence

import factory
from django.contrib.auth import get_user_model
from factory import Faker, post_generation
from factory.django import DjangoModelFactory

from backend.chat.models import Message, Room, RoomMember
from backend.users.models import Contact
from backend.utils.hasher import generate_hash


class UserFactory(DjangoModelFactory):

    username = Faker("user_name")
    email = Faker("email")
    first_name = Faker("first_name")
    last_name = Faker("last_name")

    @post_generation
    def password(self, create: bool, extracted: Sequence[Any], **kwargs):
        if not extracted:
            password = (
                extracted
                if extracted
                else Faker(
                    "password",
                    length=42,
                    special_chars=True,
                    digits=True,
                    upper_case=True,
                    lower_case=True,
                ).evaluate(None, None, extra={"locale": None})
            )
        else:
            password = extracted

        self.set_password(password)

    class Meta:
        model = get_user_model()
        django_get_or_create = ["username", "email"]


class RoomFactory(DjangoModelFactory):

    owner = factory.SubFactory(UserFactory)
    name = Faker("user_name")
    type = Room.RoomType.group_chat.name
    hash = generate_hash

    class Meta:
        model = Room
        django_get_or_create = ["owner", "name", "type", "hash"]


class RoomMemberFactory(DjangoModelFactory):

    user = factory.SubFactory(UserFactory)
    room = factory.SubFactory(RoomFactory)

    class Meta:
        model = RoomMember
        django_get_or_create = ["user", "room"]


class MessageFactory(DjangoModelFactory):

    sender = factory.SubFactory(UserFactory)
    room = factory.SubFactory(RoomFactory)
    content = Faker("sentence")

    class Meta:
        model = Message
        django_get_or_create = ["sender", "room", "content"]


class ContactFactory(DjangoModelFactory):

    user = factory.SubFactory(UserFactory)
    friend = factory.SubFactory(UserFactory)

    class Meta:
        model = Contact
        django_get_or_create = ["user", "friend"]
