import logging
from smtplib import SMTPException

from django.conf import settings

from backend.utils.tasks import send_email_backend

logger = logging.getLogger(__name__)


class EmailService:
    @classmethod
    def send_reset_password_email(cls, recipients, token):
        message = f"Your token: {token}"

        cls._send_email("Restore password", message, recipients)

    @staticmethod
    def _send_email(subject, message, recipients):
        try:
            send_email_backend.delay(
                subject, message, settings.EMAIL_HOST_USER, recipients
            )
        except SMTPException as e:
            logger.error(f"There was an error sending an email: {str(e)}")
