import secrets


def generate_hash(nbytes=16) -> str:
    return secrets.token_urlsafe(nbytes)
