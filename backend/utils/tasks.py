from django.core.mail import send_mail

from config import celery_app


@celery_app.task()
def send_email_backend(subject, message, host, recipients):
    send_mail(
        subject,
        message,
        host,
        recipients,
        fail_silently=False,
    )
