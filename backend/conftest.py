import pytest
from django.contrib.auth import get_user_model
from rest_framework.test import APIClient

from backend.utils.factories import UserFactory

User = get_user_model()


@pytest.fixture(scope="session")
def create_user(django_db_setup, django_db_blocker):
    with django_db_blocker.unblock():
        username = "test-username"
        email = f"{username}@gmail.com"
        UserFactory(username=username, email=email)


@pytest.fixture(scope="session")
def api_client_with_auth_header(create_user, django_db_blocker):
    api_client = APIClient()
    api_client.credentials(HTTP_AUTHORIZATION="Test-Token")
    with django_db_blocker.unblock():
        user = User.objects.get(email="test-username@gmail.com")
        api_client.force_authenticate(user=user, token="Test-Token")
    return api_client, user


@pytest.fixture()
def api_client_no_auth(create_user, django_db_blocker):
    client = APIClient()
    return client
