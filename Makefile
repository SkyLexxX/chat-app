setup_pre_commit: local_requirements
	pre-commit install

setup_venv:
	python3 -m venv venv

clean:
	rm -rf .cache
	rm -rf htmlcov coverage.xml .coverage
	rm -rf venv

local_requirements:
	pip3 install -r requirements/local.txt

prod_requirements:
	pip3 install -r requirements/production.txt

run_tests:
	pytest

migrate:
	python manage.py migrate --noinput

runserver:
	python manage.py runserver

collect_static:
	python manage.py collectstatic --noinput

superuser:
	python manage.py createsuperuser

redis:
	docker run -p 6379:6379 -d redis:5

local_setup: clean setup_pre_commit setup_venv local_requirements prod_requirements collect_static migrate

local_docker_setup:
	docker-compose -f local.yml build
	docker-compose -f local.yml up
	docker-compose -f local.yml run --rm django python manage.py migrate

deploy_requirements:
	apt update && apt -y install make
	pip3 install --upgrade pip
	pip3 install -r requirements/deploy.txt

init_aws_credentials: deploy_requirements
	mkdir ~/.aws/
	touch ~/.aws/credentials
	printf "[eb-cli]\naws_access_key_id = %s\naws_secret_access_key = %s\n" "$AWS_ACCESS_KEY_ID" "$AWS_SECRET_ACCESS_KEY" >> ~/.aws/credentials

deploy: deploy_requirements init_aws_credentials
	eb init $(AWS_EB_APP_NAME) -r eu-central-1 -p python
	eb use $(AWS_EB_ENVIRONMENT)
	eb deploy $(AWS_EB_ENVIRONMENT)
